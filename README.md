# aoc-2022

My approach to Advent of Code 2022 using JS to solve the puzzles.

[Leaderboard](https://adventofcode.com/2022/leaderboard/private/view/1030902)

## Usage

```bash
node aoc.js --day 3 --puzzle 1
```

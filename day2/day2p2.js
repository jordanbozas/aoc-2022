const { processLineByLine } = require('../helpers');
const _ = require('lodash');

async function runPuzzle() {
    const actions = await processLineByLine('./day2/input');
    let sum = 0;
    
    actions.map(action => {
      const score = getResult(action);
      sum += Number(score);
    });
    
    console.log(sum);
    // answer = 14184
}

function getResult(play) {
  const opponent = getShape(play.split(' ')[0]);
  const expectedResult = getExpectedResult(play.split(' ')[1]);

  const matchScore = getMatchScore(expectedResult);
  const playerScore = getShapeScore(getPlayerPlay(opponent, expectedResult));

  return matchScore + playerScore;
}

function getPlayerPlay(opponent, expectedResult) {
  if (expectedResult === 'x') {
    return opponent;
  } else if (expectedResult === '1') {
    return opponent === 'Rock' ? 'Scissors' : opponent === 'Scissors' ? 'Paper' : 'Rock';
  } else if (expectedResult === '2') {
    return opponent === 'Rock' ? 'Paper' : opponent === 'Scissors' ? 'Rock' : 'Scissors';
  }
}

function getShape(character) {
  switch (character) {
    case 'A':
      return 'Rock';
    case 'B':
      return 'Paper';
    case 'C':
      return 'Scissors';
  }
}

function getShapeScore(shape) {
  switch (shape) {
    case 'Rock':
      return 1;
    case 'Paper':
      return 2;
    case 'Scissors':
      return 3;
  }
}

function getExpectedResult(character) {
  switch (character) {
    case 'X':
      return '1';
    case 'Y':
      return 'x';
    case 'Z':
      return '2';
  }
}

function getMatchScore(matchResult) {
  switch (matchResult) {
    case 'x':
      return 3;
    case '1':
      return 0;
    case '2':
      return 6;
  }
}

module.exports = { runPuzzle };
const { processLineByLine } = require('../helpers');
const _ = require('lodash');

async function runPuzzle() {
    const actions = await processLineByLine('./day2/input');
    let sum = 0;
    
    actions.map(action => {
      const score = getResult(action);
      sum += Number(score);
    });
    
    console.log(sum);
    // answer = 13675
}

function getResult(play) {
  const opponent = getShape(play.split(' ')[0]);
  const player = getShape(play.split(' ')[1]);

  const result = getMatchResult(opponent, player);
  const matchScore = getMatchScore(result);
  const playerScore = getShapeScore(player);

  return matchScore + playerScore;
}

function getMatchResult(opponent, player) {
  if (opponent === player) {
    return 'x';
  } else if ((opponent === 'Paper' && player === 'Rock') || 
            (opponent === 'Rock' && player === 'Scissors') || 
            (opponent === 'Scissors' && player === 'Paper')) {
    return '1';
  } else {
    return '2';
  }
}

function getShape(character) {
  switch (character) {
    case 'A':
    case 'X':
      return 'Rock';
    case 'B':
    case 'Y':
      return 'Paper';
    case 'C':
    case 'Z':
      return 'Scissors';
  }
}

function getShapeScore(shape) {
  switch (shape) {
    case 'Rock':
      return 1;
    case 'Paper':
      return 2;
    case 'Scissors':
      return 3;
  }
}

function getMatchScore(matchResult) {
  switch (matchResult) {
    case 'x':
      return 3;
    case '1':
      return 0;
    case '2':
      return 6;
  }
}

module.exports = { runPuzzle };
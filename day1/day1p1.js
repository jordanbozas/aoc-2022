const { processLineByLine } = require('../helpers');
const _ = require('lodash');

async function runPuzzle() {
    const calories = await processLineByLine('./day1/input');
    const elfs = [];
    let elfIndex = 0;
    
    calories.map(calory => {
        if (!elfs[elfIndex] && calory.length) {
            elfs.push({
                name: `Elfo-${elfIndex}`,
                index: elfIndex,
                caloriesSum: 0,
                calories: []

            });
        }

        if (calory.length && elfs[elfIndex]) {
            elfs[elfIndex].calories.push(calory);
            elfs[elfIndex].caloriesSum += Number(calory);
        } else {
            elfIndex++;
        }
    });

    const maxElf = _.maxBy(elfs, 'caloriesSum');
    
    console.log(maxElf.caloriesSum);
    // answer = 66616
}

module.exports = { runPuzzle };
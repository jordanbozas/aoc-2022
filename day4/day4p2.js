const { processLineByLine } = require('../helpers');
const _ = require('lodash');

async function runPuzzle() {
    const pairs = await processLineByLine('./day4/input');
    let counter = 0;
    
    pairs.map(pair => {
      const firstPair = pair.split(',')[0];
      const secondPair = pair.split(',')[1];
      const firstNumberList = getNumberList(firstPair);
      const secondNumberList = getNumberList(secondPair);
      const intersection = _.intersection(firstNumberList, secondNumberList);
      
      if (intersection.length) {
        counter++;
      }
    });
    
    console.log(counter);
    // answer = 835
}

function getNumberList(pair) {
  const start = pair.split('-')[0];
  const finish = pair.split('-')[1];
  const numberList = [];

  for(let index = Number(start); index <= Number(finish); index++) {
    numberList.push(index);
  }

  return numberList;
}

module.exports = { runPuzzle };
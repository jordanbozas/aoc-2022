const { processLineByLine } = require('../helpers');
const _ = require('lodash');

async function runPuzzle() {
    const commands = await processLineByLine('./day7/input');
    let currentDirectory = '';
    let currentPath = '';
    const directories = [{
        dir: '/',
        parent: '',
        path: '/',
        files: []
    }];


    commands.forEach(command => {
        if (_.startsWith(command, '$ cd')) {
            const directory = _.last(command.split(' '));
            if (directory === '..') {
                const workingDir = _.find(directories, { 'dir': currentDirectory, 'path': currentPath });
                const pathArray = _.compact(currentPath.split('/'));
                pathArray.pop();
                currentPath = pathArray.length ? '/' + pathArray.join('/') : '/';
                currentDirectory = workingDir ? workingDir.parent : '/';
            } else if (directory === '/') {
                currentPath = '/';
                currentDirectory = '/';
            } else {
                const newPath = currentPath === '/' ? directory : '/' + directory;
                currentPath =  currentPath + newPath;
                currentDirectory = directory;
            }
        } else if (_.startsWith(command, '$ ls')) {
            //console.log(currentPath);
        } else if (_.startsWith(command, 'dir')) {
            const directory = _.last(command.split(' '));
            const newSavedPath = currentPath === '/' ? directory : '/' + directory;
            const savedDirectory = _.find(directories, { 'dir': directory, 'path': currentPath + newSavedPath });
            if (!savedDirectory) {
                directories.push({
                    dir: directory,
                    parent: currentDirectory,
                    path: currentPath + newSavedPath,
                    files: []
                });
            }
        } else {
            const workingDirectory = _.find(directories, { 'dir': currentDirectory, 'path': currentPath });
            const file = command.split(' ');
            workingDirectory.files.push({
                name: file[1],
                size: Number(file[0])
            });
        }
    });

    const directoriesWithSize = directories.map(directory => {
        return _.assign({}, directory, { size: getDirSize(directory, directories) });
    });

    const maxDirSize = _.maxBy(directoriesWithSize, 'size');
    const freeSpace = 70000000 - maxDirSize.size;
    const neededSpace = 30000000 - freeSpace;
    const enoughSpaceDirectories = _.filter(directoriesWithSize, (dir) => dir.size >= neededSpace);
    const minDirectory = _.minBy(enoughSpaceDirectories, 'size');
    console.log(minDirectory.size);
    // answer = 3842121
}

function getDirSize(directory, directories) {
    const children = _.filter(directories, (dir) => {
        return directory.path !== dir.path && _.startsWith(dir.path, directory.path);
    });
    const fileSize = _.sumBy(directory.files, 'size');

    const calculatedChildren = children.map(child => {
        return {
            childPath: child.path,
            size: _.sumBy(child.files, 'size')
        }
    });
    const childrenSize = _.sumBy(calculatedChildren, 'size');

    return Number(fileSize) + Number(childrenSize);
}

module.exports = { runPuzzle };
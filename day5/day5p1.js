const { processLineByLine } = require('../helpers');
const _ = require('lodash');

async function runPuzzle() {
    const input = await processLineByLine('./day5/input');
    let cargo = [];
    let numbering;
    let plan = [];
    
    input.forEach(line => {
      if (line.includes('[')) {
        cargo.push(line);
      } else if (line.includes('move')) {
        plan.push(line);
      } else if (line.length) {
        numbering = line;
      }
    });

    const stacks = loadStacks(cargo, numbering);
    const processed = processStacks(stacks, plan);

    let top = '';
    processed.forEach(stack => {
      const topEntry = stack.cargo.pop();
      top += topEntry.replace('[', '').replace(']', '');
    })

    console.log(top);    
    // answer = VGBBJCRMN
}

function loadStacks(cargo, numbering) {
  let rows = [];
  for (let index = cargo.length - 1; index >= 0; index--) {
    const element = cargo[index];
    rows.push(_.compact(element.replace(/    /g, ' [] ').split(' ')));
  }

  let stacks = [];
  const numbers = _.compact(numbering.split(' ')).length;
  for (let stack = 0; stack < numbers; stack++) {
    const column = {
      number: stack + 1,
      cargo: []
    };
    rows.forEach(row => {
      if (row.length > stack && row[stack] !== '[]') {
        column.cargo.push(row[stack]);
      }
    });
    stacks.push(column);
  }

  return stacks;
}

function processStacks(stacks, plan) {
  plan.forEach(rawCommand => {
    const command = rawCommand.replace('move', '').replace('from', '').replace('to', '');
    const splitCommand = _.compact(command.split(' '));
    const quantity = splitCommand[0];
    const from = splitCommand[1];
    const to = splitCommand[2];

    for (let crate = 1; crate <= quantity; crate++) {
      const startingStack = _.find(stacks, { number: Number(from) });
      const endingStack = _.find(stacks, { number: Number(to) });
      const inTransit = startingStack.cargo.pop();
      endingStack.cargo.push(inTransit);
    }
  });

  return stacks;
}

module.exports = { runPuzzle };
const yargs = require('yargs/yargs')
const { hideBin } = require('yargs/helpers')
const argv = yargs(hideBin(process.argv)).argv

const day = argv.day || new Date().getDate();
const puzzle = argv.puzzle || 1;

const { runPuzzle } = require(`./day${day}/day${day}p${puzzle}`);

runPuzzle();

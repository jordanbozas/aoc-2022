const { processLineByLine } = require('../helpers');
const _ = require('lodash');

async function runPuzzle() {
    const trees = await processLineByLine('./day8/input');
    let visibleCounter = 0;

    for (let i = 0; i < trees.length; i++) {
      const row = trees[i];
      for (let j = 0; j < row.length; j++) {
        const tree = trees[i][j];
        const column = getColumn(trees, j);

        const top = !!_.find(column.slice(0, i), e=> Number(e) >= Number(tree));
        const bottom = !!_.find(column.slice(i+1, trees.length), e=> Number(e) >= Number(tree));

        const left = !!_.find(row.slice(0, j), e=> Number(e) >= Number(tree));
        const right = !!_.find(row.slice(j+1, row.length), e=> Number(e) >= Number(tree));
        if (!left || !right || !top || !bottom) {
          visibleCounter++;
        }
      }
    }

    console.log(visibleCounter);
    // answer = 1560
}

function getColumn(trees, j) {
  const column = [];
  for (let i = 0; i < trees.length; i++) {
    column.push(trees[i][j]);
  }

  return column;
}

module.exports = { runPuzzle };
const { processLineByLine } = require('../helpers');
const _ = require('lodash');

async function runPuzzle() {
    const trees = await processLineByLine('./day8/input');
    let maxScenicScore = 0;

    for (let i = 0; i < trees.length; i++) {
      const row = trees[i];
      for (let j = 0; j < row.length; j++) {
        const tree = trees[i][j];
        const column = getColumn(trees, j);

        const topScore = findLimit(_.reverse(_.toArray(column.slice(0, i))), tree);
        const bottomScore = findLimit(_.toArray(column.slice(i + 1, trees.length)), tree);

        const leftScore = findLimit(_.reverse(_.toArray(row.slice(0, j))), tree);
        const rightScore = findLimit(_.toArray(row.slice(j + 1, row.length)), tree);
        
        const totalScore = topScore * bottomScore * leftScore * rightScore;
        
        if (totalScore > maxScenicScore) {
          maxScenicScore = totalScore;
        } 
      }
    }

    console.log(maxScenicScore);
    // answer = 252000
}

function getColumn(trees, j) {
  const column = [];
  for (let i = 0; i < trees.length; i++) {
    column.push(trees[i][j]);
  }

  return column;
}

function findLimit(treeList, tree) {
  let limit = 0;
  for (const viewTree in treeList) {
    const viewingTree = treeList[viewTree];
    limit++;

    if (viewingTree >= tree) {
      break;
    }
  }

  return limit;
}

module.exports = { runPuzzle };
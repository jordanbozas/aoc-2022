const { processLineByLine } = require('../helpers');
const _ = require('lodash');

async function runPuzzle() {
    const input = await processLineByLine('./day6/input');
    const signal = input[0];
    let pointer = 0;

    while (pointer < signal.length) {
      if (pointer > 13) {
        const previous = signal.slice(pointer - 14, pointer)

        if (_.uniq(previous).length === previous.length) {
          break;
        }
      }
      pointer++;
    }

    console.log(pointer);
    // answer = 2974
}

module.exports = { runPuzzle };
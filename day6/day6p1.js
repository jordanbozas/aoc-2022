const { processLineByLine } = require('../helpers');
const _ = require('lodash');

async function runPuzzle() {
    const input = await processLineByLine('./day6/input');
    const signal = input[0];
    let pointer = 0;

    while (pointer < signal.length) {
      const element = signal[pointer];
      if (pointer > 2) {
        const lastThird = signal[pointer - 3];
        const lastSecond = signal[pointer - 2];
        const lastFirst = signal[pointer - 1];

        if (element !== lastThird && element !== lastSecond && element !== lastFirst
          && lastThird !== lastSecond && lastThird !== lastFirst
          && lastSecond !== lastFirst) {
          break;
        }
      }
      pointer++;
    }

    console.log(pointer + 1);
    // answer = 1760
}

module.exports = { runPuzzle };
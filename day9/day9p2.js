const { processLineByLine } = require('../helpers');
const _ = require('lodash');

async function runPuzzle() {
  const directions = await processLineByLine('./day9/input');
  let head = { x: 0, y: 0 };
  let tail = [
    { x: 0, y: 0 },
    { x: 0, y: 0 },
    { x: 0, y: 0 },
    { x: 0, y: 0 },
    { x: 0, y: 0 },
    { x: 0, y: 0 },
    { x: 0, y: 0 },
    { x: 0, y: 0 },
    { x: 0, y: 0 },
    { x: 0, y: 0 }
  ];
  const visitedCoordinates = [];

  directions.forEach(entry => {
    const direction = entry.split(' ')[0];
    const steps = Number(entry.split(' ')[1]);

    if (direction === 'R') {
      for (let i = 1; i <= steps; i++) {
        head.x++;

        let previous = head;
        for(let j = 0; j < tail.length; j++) {
          if (previous.x - tail[j].x > 1 && tail[j].y === previous.y) {
            tail[j].x++;
          } else if (previous.x - tail[j].x > 1 && tail[j].y !== previous.y) {
            tail[j].x++;
            tail[j].y = previous.y;
          }
          previous = tail[j];
        }
        visitedCoordinates.push({ x: tail[9].x, y: tail[9].y });
      }
    } else if (direction === 'U') {
      for (let i = 1; i <= steps; i++) {
        head.y++;

        let previous = head;
        for(let j = 0; j < tail.length; j++) {
          if (previous.y - tail[j].y > 1 && tail[j].x === previous.x) {
            tail[j].y++;
          } else if (previous.y - tail[j].y > 1 && tail[j].x !== previous.x) {
            tail[j].y++;
            tail[j].x = previous.x;
          }
          previous = tail[j];
        }
        visitedCoordinates.push({ x: tail[9].x, y: tail[9].y });
      }
    } else if (direction === 'L') {
      for (let i = 1; i <= steps; i++) {
        head.x--;

        let previous = head;
        for(let j = 0; j < tail.length; j++) {
          if (tail[j].x - previous.x > 1 && tail[j].y === previous.y) {
            tail[j].x--;
          } else if (tail[j].x - previous.x > 1 && tail[j].y !== previous.y) {
            tail[j].x--;
            tail[j].y = previous.y;
          }
          previous = tail[j];
        }
        visitedCoordinates.push({ x: tail[9].x, y: tail[9].y });
      }
    } else if (direction === 'D') {
      for (let i = 1; i <= steps; i++) {
        head.y--;
        
        let previous = head;
        for(let j = 0; j < tail.length; j++) {
          if (tail[j].y - previous.y > 1 && tail[j].x === previous.x) {
            tail[j].y--;
          } else if (tail[j].y - previous.y > 1 && tail[j].x !== previous.x) {
            tail[j].y--;
            tail[j].x = previous.x;
          }
          previous = tail[j];
        }
        visitedCoordinates.push({ x: tail[9].x, y: tail[9].y });
      }
    } else {
      console.log('Unknown direction');
    }
  });
  

  console.log(_.uniqWith(visitedCoordinates, _.isEqual).length);
  // answer = ???
}

function move() {

}

function isFarAway() {

}

module.exports = { runPuzzle };
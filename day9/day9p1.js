const { processLineByLine } = require('../helpers');
const _ = require('lodash');

async function runPuzzle() {
  const directions = await processLineByLine('./day9/input');
  let head = { x: 0, y: 0 };
  let tail = { x: 0, y: 0 };
  const visitedCoordinates = [];

  directions.forEach(entry => {
    const direction = entry.split(' ')[0];
    const steps = Number(entry.split(' ')[1]);

    if (direction === 'R') {
      for (let i = 1; i <= steps; i++) {
        head.x++;
        if (head.x - tail.x > 1 && tail.y === head.y) {
          tail.x++;
        } else if (head.x - tail.x > 1 && tail.y !== head.y) {
          tail.x++;
          tail.y = head.y;
        }
        visitedCoordinates.push({ x: tail.x, y: tail.y });
      }
    } else if (direction === 'U') {
      for (let i = 1; i <= steps; i++) {
        head.y++;
        if (head.y - tail.y > 1 && tail.x === head.x) {
          tail.y++;
        } else if (head.y - tail.y > 1 && tail.x !== head.x) {
          tail.y++;
          tail.x = head.x;
        }
        visitedCoordinates.push({ x: tail.x, y: tail.y });
      }
    } else if (direction === 'L') {
      for (let i = 1; i <= steps; i++) {
        head.x--;
        if (tail.x - head.x > 1 && tail.y === head.y) {
          tail.x--;
        } else if (tail.x - head.x > 1 && tail.y !== head.y) {
          tail.x--;
          tail.y = head.y;
        }
        visitedCoordinates.push({ x: tail.x, y: tail.y });
      }
    } else if (direction === 'D') {
      for (let i = 1; i <= steps; i++) {
        head.y--;
        if (tail.y - head.y > 1 && tail.x === head.x) {
          tail.y--;
        } else if (tail.y - head.y > 1 && tail.x !== head.x) {
          tail.y--;
          tail.x = head.x;
        }
        visitedCoordinates.push({ x: tail.x, y: tail.y });
      }
    } else {
      console.log('Unknown direction');
    }
  });
  

  console.log(_.uniqWith(visitedCoordinates, _.isEqual).length);
  // answer = 6332
}

function move() {

}

function isFarAway() {

}

module.exports = { runPuzzle };
const { processLineByLine } = require('../helpers');
const _ = require('lodash');

async function runPuzzle() {
    const supplies = await processLineByLine('./day3/input');
    let sum = 0;
    let index = 1;

    while (index <= supplies.length - 2) {
      const firstElf = supplies[index-1];
      const secondElf = supplies[index];
      const thirdElf = supplies[index+1];
      const intersection = _.intersection(_.toArray(firstElf), _.toArray(secondElf), _.toArray(thirdElf));
      const itemNumberSum = _.sumBy(intersection, getIndex);

      sum += Number(itemNumberSum);
      index+=3;
    }
    
    console.log(sum);
    // answer = 2548
}

function getIndex(character) {
  const lowers = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
  const capitals = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
  const lowerIndex = _.indexOf(lowers, character);
  const capitalIndex = _.indexOf(capitals, character);

  if (lowerIndex >= 0) {
    return 1 + lowerIndex;
  } else if (capitalIndex >= 0) {
    return 27 + capitalIndex;
  } else {
    return 0;
  }
}

module.exports = { runPuzzle };